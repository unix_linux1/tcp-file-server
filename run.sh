#!/bin/bash

FILE=myfile.txt
HOSTNAME=127.0.0.1

# Start file server
./file_server &

# Loop through requests
for i in {1..5}; do
    echo -e "Client ${i} requesting $FILE\n"
    ./client_server $HOSTNAME $FILE new_file${i}.txt
done
