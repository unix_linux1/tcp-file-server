/**************************************************************************/
/* Program demonstrates a simple TCP client connection.  */
/**************************************************************************/

#include <asm-generic/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>

#define FALSE       0
#define TRUE        1
#define PORT        8888
#define BUFFER_SIZE 1024

int main(int argc, char* argv[])
{
    int client_sockfd, rc;
    char buffer[BUFFER_SIZE] = { 0 };

    if (argc != 4 || strcmp(argv[1], "--help") == 0)
    {
        fprintf(stderr, "Usage: %s [ip-address] [filepath_requested] [new_filepath]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Create a socket for client
    client_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_sockfd < 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }


    // Assign passed param to server addr
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(PORT);
    // Convert IPv4 adress to binary
    rc = inet_pton(AF_INET, argv[1], &serveraddr.sin_addr);
    if (rc < 0)
    {
        perror("inet_pton() failed");
        exit(EXIT_FAILURE);
    }

    // Establish connection with server
    rc = connect(client_sockfd, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
    if (rc < 0)
    {
        perror("connect() failed");
        exit(EXIT_FAILURE);
    }

    printf("[Client] requesting DATA TRANSFER");
    ssize_t nbytes_read = send(client_sockfd, argv[2], strlen(argv[2]), 0);
    if (nbytes_read < 0)
    {
        perror("send() failed");
        exit(EXIT_FAILURE);
    }
    
    // Open / Create file to write in
    int open_flags = O_CREAT | O_WRONLY | O_TRUNC;
    mode_t file_perms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                S_IROTH | S_IWOTH;
    int file_fd = open(argv[3], open_flags, file_perms);
    if (file_fd < 0)
    {
        perror("  open() failed");
        exit(EXIT_FAILURE);
    }

    // Write data transferred to file
    int incomplete_transfer = FALSE;
    while ((nbytes_read = recv(client_sockfd, buffer, BUFFER_SIZE, 0)) >= 0)
    {
        if (nbytes_read == 0)
        {
            break;
        }

        if (write(file_fd, buffer, nbytes_read) != nbytes_read)
        {
            fprintf(stderr, "write() failed or partial write occured");
            incomplete_transfer = TRUE;
        }        
    }

    if (incomplete_transfer)
    {
        fprintf(stderr, "WARNING: write to file incomplete!");
    }
    rc = close(file_fd);
    if (rc < 0)
    {
        perror("  close() failed");
    }
    printf("[Client] Data transfer complete, closing connection.\n");

    close(client_sockfd);
    exit(EXIT_SUCCESS);
}