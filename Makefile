all: file_server client_server

clean:
	rm -f file_server client_server

file_server: file_server.c
	gcc -o file_server file_server.c -Wall

client_server: client_server.c
	gcc -o client_server client_server.c -Wall