# TCP File Server

This TCP file server facilitates file transfer between mulitiple clients and the file server. It listens for incoming connections, accepts file requests from clients, and transfers the requested files to clients. Below is an overview of its functionality:

![system calls between server and client](tcp_file_server.png)

## Features

- Socket Creation and Configuration: The server creates a `socket()` to listen for incoming connections and configures it for non-blocking operation using `ioctl()`.
- Connection Handling: It accepts incoming connections from clients using `connect()` and manages multiple connections concurrently using the `select()` system call.
- File Transfer: Upon receiving a file request from a client, the server opens the requested file, reads its contents, and sends them to the client over the established connection.


## Usage
- Compile code with `make` command

- Execute script to start file server and simulate client requests
`./run.sh`